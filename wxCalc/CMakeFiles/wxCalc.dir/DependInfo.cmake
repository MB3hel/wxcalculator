# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  "RC"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "F:/WXC++/wxcalculator/wxCalc/calculatorgui.cpp" "F:/WXC++/wxcalculator/wxCalc/CMakeFiles/wxCalc.dir/calculatorgui.cpp.obj"
  "F:/WXC++/wxcalculator/wxCalc/main.cpp" "F:/WXC++/wxcalculator/wxCalc/CMakeFiles/wxCalc.dir/main.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "WXUSINGDLL"
  "_FILE_OFFSET_BITS=64"
  "__WXMSW__"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "C:/msys64/mingw32/lib/wx/include/msw-unicode-3.1"
  "C:/msys64/mingw32/include/wx-3.1"
  "icons"
  "resource-files"
  )
set(CMAKE_DEPENDS_CHECK_RC
  "F:/WXC++/wxcalculator/wxCalc/win_res.rc" "F:/WXC++/wxcalculator/wxCalc/CMakeFiles/wxCalc.dir/win_res.rc.obj"
  )

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_RC
  "WXUSINGDLL"
  "_FILE_OFFSET_BITS=64"
  "__WXMSW__"
  )

# The include file search paths:
set(CMAKE_RC_TARGET_INCLUDE_PATH
  "C:/msys64/mingw32/lib/wx/include/msw-unicode-3.1"
  "C:/msys64/mingw32/include/wx-3.1"
  "icons"
  "resource-files"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
