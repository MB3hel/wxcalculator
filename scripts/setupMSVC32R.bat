@Echo off
cd %~dp0\..
rmdir /s /q build
mkdir build
cd build

conan install .. -s arch=x86 -s compiler.runtime=MT -s build_type=Release --build missing
cmake .. -G "Visual Studio 15 2017"

@Rem CMAKE_BUILD_TYPE does not affect multi config generators such as VS
echo.Release>cmake_build_config.txt