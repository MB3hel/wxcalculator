///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jan 23 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __CALCULATORGUI_H__
#define __CALCULATORGUI_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/textctrl.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class CalculatorFrameBase
///////////////////////////////////////////////////////////////////////////////
class CalculatorFrameBase : public wxFrame 
{
	private:
	
	protected:
		wxTextCtrl* input;
		wxButton* btnClr;
		wxButton* btn7;
		wxButton* btn8;
		wxButton* btn9;
		wxButton* btnAdd;
		wxButton* btn4;
		wxButton* btn5;
		wxButton* btn6;
		wxButton* btnSub;
		wxButton* btn1;
		wxButton* btn2;
		wxButton* btn3;
		wxButton* btnMul;
		wxButton* btnNeg;
		wxButton* btnDec;
		wxButton* btn0;
		wxButton* btnDiv;
		wxStaticText* placeholder1;
		wxStaticText* placeholder2;
		wxStaticText* placeholder3;
		wxButton* btnEqu;
		
		// Virtual event handlers, overide them in your derived class
		virtual void onClick( wxCommandEvent& event ) { event.Skip(); }
		
	
	public:
		
		CalculatorFrameBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Calculator"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 350,250 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		
		~CalculatorFrameBase();
	
};

#endif //__CALCULATORGUI_H__
