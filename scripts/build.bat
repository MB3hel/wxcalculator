@Echo off
SETLOCAL EnableDelayedExpansion

cd %~dp0\..\build\

@Rem Multiconfig generators (like visual studio) do not work with CMAKE_BUILD_TYPE
@Rem Instead the --config flag must be used. The setup scripts write the config to a file.
@Rem this reads from that file and uses the config (if the file exists)

set CONFIG_FLAG=
if EXIST cmake_build_config.txt (
    SET /P CFG=<cmake_build_config.txt
    set CONFIG_FLAG=--config !CFG!
)

cmake --build . %CONFIG_FLAG%