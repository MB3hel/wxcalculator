@Echo off

cd %~dp0/../
rmdir /s /q build
mkdir build
cd build

conan install .. -s arch=x86_64 -s compiler=gcc -s compiler.version=7.1 -s compiler.libcxx=libstdc++11 -s compiler.exception=seh --build missing