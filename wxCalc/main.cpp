#include "wx/wxprec.h"
#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <wx/stdpaths.h>
#include <wx/custombgwin.h>

#include <iostream>
#include <string>
#include "calculatorgui.h"

// Images and embedded resource files
#include "resources.h"
#include "icon.xpm"

using namespace std;

#ifdef __WINDOWS__
#include <Windows.h>
#endif

wxString removeZero(wxString str){
    str.erase ( str.find_last_not_of('0') + 1, std::string::npos );
    if (str.EndsWith("."))
      str.RemoveLast();
    return str;
}

////////////////////////////////////////
// CalculatorFrame with event functions
////////////////////////////////////////
class CalculatorFrame : public CalculatorFrameBase {
public:
	// Class variables
	double firstNum = 0;
	wxString currentOp = wxT("");
	bool clear = false; // Clear on next button press
	// Constructor Pass Through
	CalculatorFrame(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Calculator"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(350, 250), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL) : CalculatorFrameBase(parent, id, title, pos, size, style) {	}
	// Events
	virtual void onClick(wxCommandEvent& event);
	// Class functions
	double solve(double num2);
};

void CalculatorFrame::onClick(wxCommandEvent& e) {
	wxButton *src = wxDynamicCast(e.GetEventObject(), wxButton); // The button
	// Clear the text if needed (should only be after = button)
	if (this->clear && src != this->btnAdd && src != this->btnSub && src != this->btnMul && src != this->btnDiv) {
		this->clear = false;
		this->input->Clear();
	}
	wxString origText = this->input->GetValue();
	wxString newText = wxT("");
	// Handle the event based on the button
	if (src == this->btnNeg) {
		if (origText.Contains(wxT("-"))) {
			newText = origText.substr(1, origText.length() - 1); // Get rid of the leading - sign
		} else {
			newText = wxT("-") + origText; // Add a - sign
		}
	}
    else if (src == this->btnClr) {
		newText = wxT(""); // Clear the input field
		this->currentOp = wxT("");
	}
	else if (src == this->btnDec) {
		wxString decChar = src->GetLabelText();  // Get the decimal char (could be '.' or ',')
		if (!origText.Contains(decChar)) {
			newText = origText + decChar; // Add the decimal
		} else {
			newText = origText; // We already have one decimal
		}
	}
	else if (src == this->btnAdd || src == this->btnSub || src == this->btnMul || src == this->btnDiv) {
		if (!this->currentOp.Matches(wxT(""))) {
			// There is a pending operation so solve then set new operation
			double num2;
			this->input->GetValue().ToDouble(&num2);
			this->firstNum = this->solve(num2);
		} else {
			this->input->GetValue().ToDouble(&(this->firstNum));
		}
		this->currentOp = src->GetLabelText();
		newText = wxT("");
	}
	else if (src == this->btnEqu) {
		double num2;
		this->input->GetValue().ToDouble(&num2);
		double ans = this->solve(num2);
		this->currentOp = wxT("");
		newText = removeZero(wxString::Format(wxT("%f"), ans));
		this->clear = true;
	}
	else {
		newText = origText + src->GetLabelText();  // Add the button's text to the input's text
	}
	// Set the new text
    this->input->ChangeValue(newText);
}

// Solve the operation
double CalculatorFrame::solve(double num2) {
	double value = 0;
	if (this->currentOp == wxT("+")) {
		value = this->firstNum + num2;
	}
	else if (this->currentOp == wxT("-")) {
		value = this->firstNum - num2;
	}
	else if (this->currentOp == wxT("*")) {
		value = this->firstNum * num2;
	}
	else if (this->currentOp == wxT("/")) {
		value = this->firstNum / num2;
	}
	return value;
}

////////////////////////////////////////
// Main App class
////////////////////////////////////////
class Calculator : public wxApp {
public:
	virtual bool OnInit(); // Override functions
};

IMPLEMENT_APP(Calculator); // Generate the app

// The app has started
bool Calculator::OnInit() {
	if (!wxApp::OnInit())
		return false;
#if defined __WINDOWS__ && defined _DEBUG
	// Open a console window to show cout and cerr messages on windows (Visual Studio does not show these)
	AllocConsole();
	freopen("CONOUT$", "w", stdout);
	freopen("CONOUT$", "w", stderr);
#endif

	// Make sure all image handlers are initialized before trying to load images
	wxInitAllImageHandlers();

	 //wxBitmap bmp = wxBITMAP_PNG_FROM_DATA(myimage);

	CalculatorFrame *mainFrame = new CalculatorFrame(NULL);
	mainFrame->SetIcon(wxICON(icon));
	mainFrame->Show(true);

	return true;
}
