project(wxCalculator)

cmake_minimum_required(VERSION 2.8)
cmake_policy(SET CMP0043 NEW)

set (CMAKE_CXX_STANDARD 11)


# Include ucm (Usefull Cmake Macros for setting MSVC runtime)
include(${PROJECT_SOURCE_DIR}/cmake/ucm.cmake)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup(TARGETS)

# Use the same runtime (MSVC MD[d] or MT[d]) as conan was configured to use
if(MSVC AND ("${CONAN_SETTINGS_COMPILER_RUNTIME}" MATCHES "MT" OR "${CONAN_SETTINGS_COMPILER_RUNTIME}" MATCHES "MTd"))
	ucm_set_runtime(STATIC) # This will use /MT and /MTd with MSVC
elseif(MSVC)
	ucm_set_runtime(DYNAMIC) # This will use /MD and /MDd with MSVC
endif()

# Build the resource generator
add_subdirectory(resources)

# Build the main program
add_subdirectory(wxCalc)

########################################################################
# Generic CPack setup
########################################################################

set(CPACK_PACKAGE_NAME "wxCalculator")
set(CPACK_PACKAGE_VENDOR "Marcus Behel")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Cross platform GUI calculator")
set(CPACK_PACKAGE_VERSION_MAJOR "1")
set(CPACK_PACKAGE_VERSION_MINOR "0")
set(CPACK_PACKAGE_VERSION_PATCH "0")
set(CPACK_PACKAGE_VERSION "${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")

########################################################################
# Windows Installer setup
########################################################################
if(WIN32)
    ########################################################################
    # Data files:
    # Install data files. These can belong to multiple components.
    # The About document is the "Manual" component
    # The extras directory is the "extras" component
    ########################################################################
    set(EXTRA_DATA_DIR data/extras)
    file(GLOB EXTRA_FILES "${CMAKE_SOURCE_DIR}/${EXTRA_DATA_DIR}/*")
    install(FILES ${EXTRA_FILES}
            DESTINATION ${EXTRA_DATA_DIR}
            COMPONENT extras)
    install(FILES "${CMAKE_SOURCE_DIR}/data/About.rtf"
            DESTINATION data
            COMPONENT manual)
    include(InstallRequiredSystemLibraries) # This takes care of everything when MSVC is used with static linking
    install(FILES ${CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS}
            DESTINATION programs
            COMPONENT applications)
    ########################################################################
    # CPack Configuration
    ########################################################################
    set(CPACK_PACKAGE_INSTALL_DIRECTORY "wxCalculator")
    #
    # Tell CPack about the components and group the data components together (CPACK_COMPONENT_${COMPONENT_NAME_ALL_CAPS}_GROUP).
    #
    set(CPACK_COMPONENTS_ALL applications manual extras)

    set(CPACK_COMPONENT_APPLICATIONS_GROUP "core")
    set(CPACK_COMPONENT_MANUAL_GROUP "data")
    set(CPACK_COMPONENT_EXTRAS_GROUP "data")
    #
    # More descriptive names for each of the components, and component groups
    #
    set(CPACK_COMPONENT_APPLICATIONS_DISPLAY_NAME "Main Program")
    set(CPACK_COMPONENT_APPLICATIONS_DESCRIPTION "The programs that makeup the core functionality.")
    set(CPACK_COMPONENT_APPLICATIONS_REQUIRED ON)

    set(CPACK_COMPONENT_MANUAL_DISPLAY_NAME "Manual")
    set(CPACK_COMPONENT_MANUAL_DESCRIPTION "A document explaining the program.")
    set(CPACK_COMPONENT_MANUAL_DISABLED ON)

    set(CPACK_COMPONENT_EXTRAS_DISPLAY_NAME "Extras")
    set(CPACK_COMPONENT_EXTRAS_DESCRIPTION "Additional, and useless, text files.")
    set(CPACK_COMPONENT_EXTRAS_DISABLED ON)

    set(CPACK_COMPONENT_GROUP_CORE_DISPLAY_NAME "Core Components")
    set(CPACK_COMPONENT_GROUP_CORE_DESCRIPTION "Essential parts of the program.")
    set(CPACK_COMPONENT_GROUP_CORE_REQUIRED ON)

    set(CPACK_COMPONENT_GROUP_DATA_DISPLAY_NAME "Other Data")
    set(CPACK_COMPONENT_GROUP_DATA_DESCRIPTION "Extra, non-essential parts of the program.")
    set(CPACK_COMPONENT_GROUP_DATA_DISABLED ON)
    #
    # Text from "license.txt" is displayed in the installer's license tab
    #
    set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/license.txt")
    #
    # Set the icon used inside the installer wizard and the icons for the installer and un-installer
    #
    set(CPACK_PACKAGE_ICON "${CMAKE_SOURCE_DIR}\\\\icons\\\\icon.ico")

    set(CPACK_NSIS_MUI_ICON "${CMAKE_SOURCE_DIR}/icons/icon.ico")
    set(CPACK_NSIS_MUI_UNIICON "${CMAKE_SOURCE_DIR}/icons/icon.ico")
    #
    # Set the programs displayed under the Start menu shortcut
    # We do not use CPACK_PACKAGE_EXECUTABLES because it expects the binaries to be in a
    # directory named "bin" (CMake bug tracker ID: 0007829).
    #
    set(CPACK_NSIS_MENU_LINKS "programs\\\\wxCalc" "wxCalculator")
endif()

if(APPLE)
    set(EXTRA_DATA_DIR data/extras)
    file(GLOB EXTRA_FILES "${CMAKE_SOURCE_DIR}/${EXTRA_DATA_DIR}/*")
    install(FILES ${EXTRA_FILES}
            DESTINATION ${EXTRA_DATA_DIR})
    install(FILES "${CMAKE_SOURCE_DIR}/data/About.rtf"
            DESTINATION data)
    install(FILES "${CMAKE_SOURCE_DIR}/license.txt"
            DESTINATION .)
endif()

include(CPack)
