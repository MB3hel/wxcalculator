///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jan 23 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "calculatorgui.h"

///////////////////////////////////////////////////////////////////////////

CalculatorFrameBase::CalculatorFrameBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	this->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW ) );
	
	wxBoxSizer* contentSizer;
	contentSizer = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* topPanel;
	topPanel = new wxBoxSizer( wxHORIZONTAL );
	
	input = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	topPanel->Add( input, 1, wxALL|wxEXPAND, 2 );
	
	btnClr = new wxButton( this, wxID_ANY, wxT("Clear"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	topPanel->Add( btnClr, 0, wxALL, 2 );
	
	
	contentSizer->Add( topPanel, 0, wxEXPAND, 5 );
	
	wxFlexGridSizer* buttonGrid;
	buttonGrid = new wxFlexGridSizer( 5, 4, 0, 0 );
	buttonGrid->AddGrowableCol( 0 );
	buttonGrid->AddGrowableCol( 1 );
	buttonGrid->AddGrowableCol( 2 );
	buttonGrid->AddGrowableCol( 3 );
	buttonGrid->AddGrowableRow( 0 );
	buttonGrid->AddGrowableRow( 1 );
	buttonGrid->AddGrowableRow( 2 );
	buttonGrid->AddGrowableRow( 3 );
	buttonGrid->AddGrowableRow( 4 );
	buttonGrid->SetFlexibleDirection( wxVERTICAL );
	buttonGrid->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	btn7 = new wxButton( this, wxID_ANY, wxT("7"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btn7, 0, wxALL|wxEXPAND, 2 );
	
	btn8 = new wxButton( this, wxID_ANY, wxT("8"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btn8, 0, wxALL|wxEXPAND, 2 );
	
	btn9 = new wxButton( this, wxID_ANY, wxT("9"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btn9, 0, wxALL|wxEXPAND, 2 );
	
	btnAdd = new wxButton( this, wxID_ANY, wxT("+"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btnAdd, 0, wxALL|wxEXPAND, 2 );
	
	btn4 = new wxButton( this, wxID_ANY, wxT("4"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btn4, 0, wxALL|wxEXPAND, 2 );
	
	btn5 = new wxButton( this, wxID_ANY, wxT("5"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btn5, 0, wxALL|wxEXPAND, 2 );
	
	btn6 = new wxButton( this, wxID_ANY, wxT("6"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btn6, 0, wxALL|wxEXPAND, 2 );
	
	btnSub = new wxButton( this, wxID_ANY, wxT("-"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btnSub, 0, wxALL|wxEXPAND, 2 );
	
	btn1 = new wxButton( this, wxID_ANY, wxT("1"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btn1, 0, wxALL|wxEXPAND, 2 );
	
	btn2 = new wxButton( this, wxID_ANY, wxT("2"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btn2, 0, wxALL|wxEXPAND, 2 );
	
	btn3 = new wxButton( this, wxID_ANY, wxT("3"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btn3, 0, wxALL|wxEXPAND, 2 );
	
	btnMul = new wxButton( this, wxID_ANY, wxT("*"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btnMul, 0, wxALL|wxEXPAND, 2 );
	
	btnNeg = new wxButton( this, wxID_ANY, wxT("(-)"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btnNeg, 0, wxALL|wxEXPAND, 2 );
	
	btnDec = new wxButton( this, wxID_ANY, wxT("."), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btnDec, 0, wxALL|wxEXPAND, 2 );
	
	btn0 = new wxButton( this, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, 0 );
	buttonGrid->Add( btn0, 0, wxALL|wxEXPAND, 2 );
	
	btnDiv = new wxButton( this, wxID_ANY, wxT("/"), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btnDiv, 0, wxALL|wxEXPAND, 2 );
	
	placeholder1 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	placeholder1->Wrap( -1 );
	buttonGrid->Add( placeholder1, 0, wxALL|wxEXPAND, 5 );
	
	placeholder2 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	placeholder2->Wrap( -1 );
	buttonGrid->Add( placeholder2, 0, wxALL|wxEXPAND, 5 );
	
	placeholder3 = new wxStaticText( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	placeholder3->Wrap( -1 );
	buttonGrid->Add( placeholder3, 0, wxALL|wxEXPAND, 5 );
	
	btnEqu = new wxButton( this, wxID_ANY, wxT("="), wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT );
	buttonGrid->Add( btnEqu, 0, wxALL|wxEXPAND, 2 );
	
	
	contentSizer->Add( buttonGrid, 1, wxEXPAND, 5 );
	
	
	this->SetSizer( contentSizer );
	this->Layout();
	
	this->Centre( wxBOTH );
	
	// Connect Events
	btnClr->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn7->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn8->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn9->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnAdd->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn4->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn5->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn6->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnSub->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn1->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn2->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn3->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnMul->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnNeg->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnDec->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn0->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnDiv->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnEqu->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
}

CalculatorFrameBase::~CalculatorFrameBase()
{
	// Disconnect Events
	btnClr->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn7->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn8->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn9->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnAdd->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn4->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn5->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn6->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnSub->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn1->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn2->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn3->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnMul->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnNeg->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnDec->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btn0->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnDiv->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	btnEqu->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( CalculatorFrameBase::onClick ), NULL, this );
	
}
