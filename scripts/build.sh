#!/bin/bash

cd "$(dirname "$0")"/../build

# Multiconfig generators (like visual studio & Xcode) do not work with CMAKE_BUILD_TYPE
# Instead the --config flag must be used. The setup scripts write the config to a file.
# this reads from that file and uses the config (if the file exists)
CONFIG_FLAG=
if [ -f cmake_build_config.txt ]; then
    CFG=$(<cmake_build_config.txt)
	CONFIG_FLAG="--config $CFG"
fi

cmake --build . $CONFIG_FLAG